use std::cmp::Ordering;

#[cfg(test)]
mod tests {
  use crate::Hillside;

  #[test]
  fn solution_part1() {
    let hillside = Hillside::from_file("input.txt");

    assert_eq!(211, hillside.sled_run(3, 1).trees_encountered());
  }

  #[test]
  fn solution_part2() {
    let hillside = Hillside::from_file("input.txt");

    let paths = vec![
      (1, 1),
      (3, 1),
      (5, 1),
      (7, 1),
      (1, 2),
    ];

    let product = paths.iter()
      .fold(1, |acc, (right, down)| acc * hillside.sled_run(*right, *down).trees_encountered());

    assert_eq!(3584591857, product);
  }
}

pub struct Hillside {
  levels: Vec<String>
}

pub struct SledRun<'a> {
  horizontal_index: i32,
  vertical_index: usize,
  horizontal_step: i32,
  vertical_step: i32,
  hillside: &'a Hillside,
}

impl Hillside {
  pub fn sled_run(&self, horizontal_step: i32, vertical_step: i32) -> SledRun {
    SledRun{
      horizontal_index: 0,
      vertical_index: 0,
      horizontal_step,
      vertical_step,
      hillside: &self,
    }
  }

  pub fn from_file(path: &str) -> Self {
    let levels: Vec<String> = std::fs::read_to_string(path)
      .expect("Unable to read input file")
      .lines()
      .map(|line| line.to_string())
      .collect();

    Hillside {
      levels
    }
  }
}

impl Iterator for SledRun<'_> {
  type Item = char;

  fn next(&mut self) -> Option<Self::Item> {
    match self.vertical_index.cmp(&self.hillside.levels.len()) {
      Ordering::Less => {
        let result = self.hillside.levels[self.vertical_index].chars().nth(self.horizontal_index as usize);

        // Advance the iterator
        self.horizontal_index = (self.horizontal_index + self.horizontal_step) % self.hillside.levels[0].len() as i32;
        self.vertical_index += self.vertical_step as usize;

        result
      },
      _ => None
    }
  }
}

impl SledRun<'_> {
  pub fn trees_encountered(&mut self) -> usize {
    self.filter(|space| *space == '#')
      .count()
  }
}
