use std::collections::HashMap;
use std::convert::TryInto;
use std::fs;
use std::str::FromStr;

#[cfg(test)]
mod tests {
    use crate::load_password_entries;
    use crate::PasswordEntry;
    use std::str::FromStr;

    #[test]
    fn get_policy1_count() {
        let password_entries = load_password_entries();
        let valid_password_count = password_entries.iter()
            .map(|s| PasswordEntry::from_str(s))
            .filter(|entry| entry.as_ref().unwrap().is_valid())
            .count();
        assert_eq!(398, valid_password_count);
    }

    #[test]
    fn get_policy2_count() {
        let password_entries = load_password_entries();
        let valid_password_count = password_entries.iter()
            .map(|s| PasswordEntry::from_str(s))
            .filter(|entry| entry.as_ref().unwrap().is_valid2())
            .count();
        assert_eq!(562, valid_password_count);
    }

    #[test]
    fn valid1() {
        assert!(PasswordEntry{min: 1, max: 3, required_rune: 'a', password: "abcde".to_string()}.is_valid());
    }
    
    #[test]
    fn invalid1() {
        assert!(!PasswordEntry{min: 1, max: 3, required_rune: 'b', password: "cdefg".to_string()}.is_valid());
    }
    
    #[test]
    fn valid2() {
        assert!(PasswordEntry{min: 2, max: 9, required_rune: 'c', password: "ccccccccc".to_string()}.is_valid());
    }

    #[test]
    fn valid2_1() {
        assert!(PasswordEntry{min: 1, max: 3, required_rune: 'a', password: "abcde".to_string()}.is_valid2());
    }

    #[test]
    fn invalid2_1() {
        assert!(!PasswordEntry{min: 1, max: 3, required_rune: 'b', password: "cdefg".to_string()}.is_valid2());
    }

    #[test]
    fn invalid2_2() {
        assert!(!PasswordEntry{min: 2, max: 9, required_rune: 'c', password: "ccccccccc".to_string()}.is_valid2());
    }
}

pub struct PasswordEntry {
    min: i32,
    max: i32,
    required_rune: char,
    password: String
}

impl FromStr for PasswordEntry {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts: Vec<&str> = s.split(": ").collect();
        if parts.len() != 2 {
            return Err("Malformed password entry");
        }
        let policy_parts: Vec<&str> = parts[0].split(" ").collect();
        if policy_parts.len() != 2 {
            return Err("Malformed password entry");
        }
        let range_parts: Vec<&str> = policy_parts[0].split("-").collect();
        let min: i32 = range_parts[0].parse().unwrap();
        let max: i32 = range_parts[1].parse().unwrap();
        let required_rune = policy_parts[1];

        Ok(PasswordEntry{
            min,
            max,
            required_rune: required_rune.chars().nth(0).unwrap(),
            password: parts[1].to_string()
        })
    }
}

impl PasswordEntry {
    pub fn is_valid(&self) -> bool {
        let mut histogram: HashMap<char, i32> = HashMap::new(); 
        for c in self.password.as_str().chars() {
            histogram.entry(c)
                .and_modify(|count| *count += 1 )
                .or_insert(1);
        }

        match histogram.get(&self.required_rune) {
            Some(&occurrences) => occurrences >= self.min && occurrences <= self.max,
            None => false
        }
    }

    pub fn is_valid2(&self) -> bool {
        let first_position = self.password.as_str().chars().nth((self.min - 1).try_into().unwrap()).unwrap();
        let second_position = self.password.as_str().chars().nth((self.max - 1).try_into().unwrap()).unwrap();

        return  (first_position == self.required_rune) ^
                (second_position == self.required_rune);
    }
}

pub fn load_password_entries() -> Vec<String> {
    fs::read_to_string("input.txt")
        .expect("Unable to read puzzle input file. Expected file is ./input.txt")
        .lines()
        .map(|line| line.to_string())
        .collect()
}
